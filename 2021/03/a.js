const fs = require('fs')

const binaries = fs.readFileSync('./input.txt', {encoding: 'utf8', flag: 'r'})
    .split('\n')
    .filter(b => b.length > 0)

let GAMMA = []
let EPSILON = []

for (let char = 11; char >= 0; char--) {
    let SUM_1 = 0
    let SUM_0 = 0

    for (let bin = 0; bin < binaries.length; bin++) {
        binaries[bin][char] === '1'
            ? SUM_1++
            : SUM_0++
    }

    if (SUM_1 > SUM_0) {
        GAMMA[char] = '1'
        EPSILON[char] = '0'
    } else {
        GAMMA[char] = '0'
        EPSILON[char] = '1'
    }
}

const gamma = GAMMA.join('')
const epsilon = EPSILON.join('')

console.log({
    gamma,
    epsilon,
    gammaDecimal: parseInt(gamma, 2),
    epsilonDecimal: parseInt(epsilon, 2),
    result: parseInt(gamma, 2) * parseInt(epsilon, 2)
})
