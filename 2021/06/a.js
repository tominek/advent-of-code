const fs = require('fs')

let fishes = fs.readFileSync('./input.txt', {encoding: 'utf8', flag: 'r'})
    .split(',')
    .map(n => Number(n))

const TOTAL_DAYS = 80

// console.log(fishes.join(','))
for (let i = 0; i < TOTAL_DAYS; i++) {
    for (let j = 0; j < fishes.length; j++) {
        if (fishes[j] !== 0) {
            fishes[j]--
        } else {
            fishes[j] = 6
            fishes.push(9)
        }
    }
    // console.log(fishes.join(','))
}

console.log('total fishes:', fishes.length)
