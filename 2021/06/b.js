const fs = require('fs')

String.prototype.replaceAt = function (index, replacement) {
    return this.substr(0, index) + replacement + this.substr(index + replacement.length);
}

let fishes = fs.readFileSync('./test.txt', {encoding: 'utf8', flag: 'r'})
    .split(',')
    .join('').replace(`\n`, '')

const TOTAL_DAYS = 18

console.log(fishes)
for (let i = 0; i < TOTAL_DAYS; i++) {
    for (let j = 0; j < fishes.length; j++) {
        if (fishes[j] !== '0') {
            fishes = fishes.replaceAt(j, Number(fishes[j]) - 1)
        } else {
            fishes = fishes.replaceAt(j, '6')
            fishes += '9'
        }
    }
    console.log(fishes)
}

console.log('total fishes:', fishes.length)
