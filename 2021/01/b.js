const fs = require('fs')

const measurements = fs.readFileSync('./input.txt', {encoding: 'utf8', flag: 'r'})
    .split('\n')
    .map(str => Number(str))

const sums = []
for (let i = 2; i < measurements.length; i++) {
    sums.push(measurements[i - 2] + measurements[i - 1] + measurements[i])
}

const increases = sums.reduce((acc, current) => {
        if (acc.prev && Number(current) !== 0 && acc.prev < Number(current)) {
            acc.sum = acc.sum + 1
        }
        acc.prev = Number(current)

        return acc
    }, {prev: null, sum: 0})

console.log(increases)


