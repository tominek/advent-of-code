const fs = require('fs')

const increases = fs.readFileSync('./input.txt', {encoding: 'utf8', flag: 'r'})
    .split('\n')
    .reduce((acc, current) => {
        console.log(acc.prev, Number(current))
        if (acc.prev && Number(current) !== 0 && acc.prev < Number(current)) {
            acc.sum = acc.sum + 1
        }
        acc.prev = Number(current)

        return acc
    }, {prev: null, sum: 0})

console.log({increases})
