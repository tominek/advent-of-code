const fs = require('fs')

const result = fs.readFileSync('./input.txt', {encoding: 'utf8', flag: 'r'})
    .split('\n')
    .reduce((position, instruction) => {
        if (instruction.length > 0) {
            const [direction, value] = instruction.split(' ')
            const X = Number(value)

            switch (direction) {
                case 'down':
                    position.aim += X
                    break
                case 'up':
                    position.aim -= X
                    break
                case 'forward':
                    position.hor += X
                    position.ver += position.aim * X
                    break
            }

            // console.log(instruction, position)
        }

        return position
    }, {hor: 0, ver: 0, aim: 0})

// console.log(result)
console.log(result.hor * result.ver)
