const fs = require('fs')

const result = fs.readFileSync('./input.txt', {encoding: 'utf8', flag: 'r'})
    .split('\n')
    .reduce((position, instruction) => {
        const [direction,value] = instruction.split(' ')

        switch (direction) {
            case 'forward':
                position.x = position.x + Number(value)
                break
            case 'down':
                position.y = position.y + Number(value)
                break
            case 'up':
                position.y = position.y - Number(value)
                break
        }

        return position
    }, {x: 0, y: 0})

console.log(result)
console.log(result.x * result.y)
