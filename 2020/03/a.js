"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// import input from './test'
var input_1 = __importDefault(require("./input"));
var Grid = /** @class */ (function () {
    function Grid() {
        this.grid = input_1.default;
        this.x = 0;
        this.y = 0;
        this.xLength = this.grid[0].length;
        this.yMax = this.grid.length - 1;
    }
    Grid.prototype.isTree = function () {
        return this.getCurrent() === Grid.TREE;
    };
    Grid.prototype.isYMax = function () {
        return this.y === this.yMax;
    };
    Grid.prototype.getCurrent = function () {
        return this.grid[this.y][this.x];
    };
    Grid.prototype.down = function (steps) {
        if (steps === void 0) { steps = 1; }
        this.y += steps;
    };
    Grid.prototype.right = function (steps) {
        if (steps === void 0) { steps = 3; }
        if (this.x + steps >= this.xLength) {
            this.x = this.x + steps - this.xLength;
        }
        else {
            this.x += steps;
        }
    };
    Grid.prototype.print = function () {
        var grid = _.cloneDeep(this.grid);
        grid[this.y][this.x] = '0';
        console.log('x:', this.x, 'y:', this.y);
        grid.forEach(function (row) { return console.log(row.join('')); });
    };
    Grid.TREE = '#';
    return Grid;
}());
function main() {
    var grid = new Grid();
    var sum = 0;
    while (!grid.isYMax()) {
        grid.right();
        grid.down();
        if (grid.isTree()) {
            sum++;
        }
        // grid.print()
    }
    console.log('TREE COUNT:', sum);
}
main();
