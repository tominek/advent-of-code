// import input from './test'
import input from './input'

class Grid {
    private static TREE = '#'
    private readonly grid: Array<Array<string>>
    private x: number
    private y: number
    private readonly xLength: number;
    private readonly yMax: number;

    constructor() {
        this.grid = input
        this.x = 0
        this.y = 0
        this.xLength = this.grid[0].length
        this.yMax = this.grid.length - 1
    }

    isTree(): boolean {
        return this.getCurrent() === Grid.TREE
    }

    isYMax(): boolean {
        return this.y === this.yMax
    }

    getCurrent(): string {
        return this.grid[this.y][this.x]
    }

    down(steps = 1): void {
        this.y += steps
    }

    right(steps = 3): void {
        if (this.x + steps >= this.xLength) {
            this.x = this.x + steps - this.xLength
        } else {
            this.x += steps
        }
    }

    print() {
        const grid = _.cloneDeep(this.grid)
        grid[this.y][this.x] = '0'
        console.log('x:', this.x, 'y:', this.y)
        grid.forEach(row => console.log(row.join('')))
    }
}

function countEncounters(right: number, down: number): number {
    const grid = new Grid()
    let sum = 0

    while (!grid.isYMax()) {
        grid.right(right)
        grid.down(down)
        if (grid.isTree()) {
            sum++
        }
    }

    return sum
}

function main() {
    const a = countEncounters(1, 1)
    const b = countEncounters(3, 1)
    const c = countEncounters(5, 1)
    const d = countEncounters(7, 1)
    const e = countEncounters(1, 2)

    console.log('Result:', a * b * c * d * e)
}

main()