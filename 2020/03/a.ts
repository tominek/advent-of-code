// import input from './test'
import input from './input'

class Grid {
    private static TREE = '#'
    private readonly grid: Array<Array<string>>
    private x: number
    private y: number
    private readonly xLength: number;
    private readonly yMax: number;

    constructor() {
        this.grid = input
        this.x = 0
        this.y = 0
        this.xLength = this.grid[0].length
        this.yMax = this.grid.length - 1
    }

    isTree(): boolean {
        return this.getCurrent() === Grid.TREE
    }

    isYMax(): boolean {
        return this.y === this.yMax
    }

    getCurrent(): string {
        return this.grid[this.y][this.x]
    }

    down(steps = 1): void {
        this.y += steps
    }

    right(steps = 3): void {
        if (this.x + steps >= this.xLength) {
            this.x = this.x + steps - this.xLength
        } else {
            this.x += steps
        }
    }

    print() {
        const grid = _.cloneDeep(this.grid)
        grid[this.y][this.x] = '0'
        console.log('x:', this.x, 'y:', this.y)
        grid.forEach(row => console.log(row.join('')))
    }
}

function main() {
    const grid = new Grid()
    let sum = 0

    while (!grid.isYMax()) {
        grid.right()
        grid.down()
        if (grid.isTree()) {
            sum++
        }
        // grid.print()
    }

    console.log('TREE COUNT:', sum)
}

main()