"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var input_1 = __importDefault(require("./input"));
// import input from './test'
var Birth_Year = 'byr';
var Issue_Year = 'iyr';
var Expiration_Year = 'eyr';
var Height = 'hgt';
var Hair_Color = 'hcl';
var Eye_Color = 'ecl';
var Passport_ID = 'pid';
var MANDATORY = [Birth_Year, Issue_Year, Expiration_Year, Height, Hair_Color, Eye_Color, Passport_ID];
function main() {
    var valid = input_1.default.filter(function (doc) {
        for (var _i = 0, MANDATORY_1 = MANDATORY; _i < MANDATORY_1.length; _i++) {
            var field = MANDATORY_1[_i];
            if (!doc.match(field)) {
                return false;
            }
        }
        return true;
    });
    console.log('VALID', valid.length);
}
main();
