import input from './input'
// import input from './test'

const Birth_Year = 'byr'
const Issue_Year = 'iyr'
const Expiration_Year = 'eyr'
const Height = 'hgt'
const Hair_Color = 'hcl'
const Eye_Color = 'ecl'
const Passport_ID = 'pid'

const MANDATORY = [Birth_Year, Issue_Year, Expiration_Year, Height, Hair_Color, Eye_Color, Passport_ID]

function main() {
    const valid = input.filter(doc => {
        for (const field of MANDATORY){
            if (!doc.match(field)) {
                return false
            }
        }
        return true
    })
    console.log('VALID', valid.length)
}

main()