import input from './input'
// import input from './test'

const BIRTH_YEAR = /byr:(?<value>\d{4}( |$))/
const ISSUE_YEAR = /iyr:(?<value>\d{4}( |$))/
const EXPIRATION_YEAR = /eyr:(?<value>\d{4}( |$))/
const HEIGHT = /hgt:(?<value>\d+)(?<unit>cm|in)( |$)/
const HAIR_COLOR = /hcl:(?<value>#[0-9a-f]{6}( |$))/
const EYE_COLOR = /ecl:(?<value>amb|blu|brn|gry|grn|hzl|oth)( |$)/
const PASSPORT_ID = /pid:(?<value>\d{9})( |$)/

function validBirthYear(doc: string): boolean {
    const match = doc.match(BIRTH_YEAR)

    return !!match && parseInt(match.groups.value) >= 1920 && parseInt(match.groups.value) <= 2002
}

function validIssueYear(doc: string): boolean {
    const match = doc.match(ISSUE_YEAR)

    return !!match && parseInt(match.groups.value) >= 2010 && parseInt(match.groups.value) <= 2020
}

function validExpirationYear(doc: string): boolean {
    const match = doc.match(EXPIRATION_YEAR)

    return !!match && parseInt(match.groups.value) >= 2020 && parseInt(match.groups.value) <= 2030
}

function validHeight(doc: string): boolean {
    const match = doc.match(HEIGHT)

    if (match) {
        const {value, unit} = match.groups
        if (unit === 'cm') {
            return parseInt(value) >= 150 && parseInt(value) <= 193
        }

        return parseInt(value) >= 59 && parseInt(value) <= 76
    }

    return false
}

function validHairColor(doc: string): boolean {
    return !!doc.match(HAIR_COLOR)
}

function validEyeColor(doc: string): boolean {
    return !!doc.match(EYE_COLOR)
}

function validPassportId(doc: string): boolean {
    return !!doc.match(PASSPORT_ID)
}

function main() {
    const valid = input.filter(doc => {
        return validBirthYear(doc)
            && validIssueYear(doc)
            && validExpirationYear(doc)
            && validHeight(doc)
            && validHairColor(doc)
            && validEyeColor(doc)
            && validPassportId(doc)
    })
    // console.log(valid)
    console.log('VALID', valid.length)
}

main()