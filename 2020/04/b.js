"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var input_1 = __importDefault(require("./input"));
// import input from './test'
var BIRTH_YEAR = /byr:(?<value>\d{4}( |$))/;
var ISSUE_YEAR = /iyr:(?<value>\d{4}( |$))/;
var EXPIRATION_YEAR = /eyr:(?<value>\d{4}( |$))/;
var HEIGHT = /hgt:(?<value>\d+)(?<unit>cm|in)( |$)/;
var HAIR_COLOR = /hcl:(?<value>#[0-9a-f]{6}( |$))/;
var EYE_COLOR = /ecl:(?<value>amb|blu|brn|gry|grn|hzl|oth)( |$)/;
var PASSPORT_ID = /pid:(?<value>\d{9})( |$)/;
function validBirthYear(doc) {
    var match = doc.match(BIRTH_YEAR);
    return !!match && parseInt(match.groups.value) >= 1920 && parseInt(match.groups.value) <= 2002;
}
function validIssueYear(doc) {
    var match = doc.match(ISSUE_YEAR);
    return !!match && parseInt(match.groups.value) >= 2010 && parseInt(match.groups.value) <= 2020;
}
function validExpirationYear(doc) {
    var match = doc.match(EXPIRATION_YEAR);
    return !!match && parseInt(match.groups.value) >= 2020 && parseInt(match.groups.value) <= 2030;
}
function validHeight(doc) {
    var match = doc.match(HEIGHT);
    if (match) {
        var _a = match.groups, value = _a.value, unit = _a.unit;
        if (unit === 'cm') {
            return parseInt(value) >= 150 && parseInt(value) <= 193;
        }
        return parseInt(value) >= 59 && parseInt(value) <= 76;
    }
    return false;
}
function validHairColor(doc) {
    return !!doc.match(HAIR_COLOR);
}
function validEyeColor(doc) {
    return !!doc.match(EYE_COLOR);
}
function validPassportId(doc) {
    return !!doc.match(PASSPORT_ID);
}
function main() {
    var valid = input_1.default.filter(function (doc) {
        return validBirthYear(doc)
            && validIssueYear(doc)
            && validExpirationYear(doc)
            && validHeight(doc)
            && validHairColor(doc)
            && validEyeColor(doc)
            && validPassportId(doc);
    });
    // console.log(valid)
    console.log('VALID', valid.length);
}
main();
