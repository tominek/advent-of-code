const fs = require('fs')
const {isEmpty} = require('lodash')

const regex = /(?<colour>\w+\s\w+)\sbag/

let bags = fs.readFileSync('./test.txt', {encoding: 'utf8', flag: 'r'})
    .split('.\n')
    .reduce((acc, b) => {
        const [p, children] = b.split('contain')
        const parent = p.match(regex).groups.colour
        const c = children.split(',').map(c => c.match(regex).groups.colour)


        acc[parent] = {}
        c.forEach(child => {
            acc[parent][child] = {}
        })

        return acc
    }, {})

console.log('RAW BAGS', bags)

let contains = Object.keys(bags).reduce((acc, colour) => {
    if (containsShiny(bags[colour])) {
        acc[colour] = bags[colour]
    }

    return acc
}, [])

console.log('CONTAINS', contains)

Object.keys(contains).forEach(colour => checkNode(bags, colour))


function containsShiny(node) {
    if (isEmpty(node)) {
        return false
    }
    if (!!node['shiny gold']) {
        return true
    }

    for (const key of Object.keys(node)) {
        containsShiny(node[key])
    }
}

function checkNode(node, containingColour) {
    if (isEmpty(node)) {
        return
    }
    if (node[containingColour]) {
        node[containingColour] = {'shiny gold': {}}
    }
    for (const key of Object.keys(node)) {
        checkNode(node[key], containingColour)
    }
}

console.log('CHECKED BAGS', bags)