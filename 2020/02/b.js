const input = require("./input")

const test = ["1-3 a: abcde", "1-3 b: cdefg", "2-9 c: ccccccccc"]

const regex = /(?<first>\d+)-(?<second>\d+)\s(?<letter>\w):\s(?<password>\w+)/

function isValid(record) {
    const {first, second, letter, password} = record.match(regex).groups
    const letters = password.split('')
    // console.log(letter, first, second, password, letters[first - 1], letters[second - 1])

    return letters[first - 1] === letter ^ letters[second - 1] === letter
}

function main() {
    const valid = input.filter(r => isValid(r))
    console.log('VALID COUNT:', valid.length)
}

main()