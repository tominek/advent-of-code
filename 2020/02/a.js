const input = require("./input")

const test = ["1-3 a: abcde", "1-3 b: cdefg", "2-9 c: ccccccccc"]

const regex = /(?<min>\d+)-(?<max>\d+)\s(?<letter>\w):\s(?<password>\w+)/

function isValid(record) {
    const {min, max, letter, password} = record.match(regex).groups
    const count = password.split(letter).length - 1

    return count >= min && count <= max
}

function main() {
    const valid = input.filter(r => isValid(r))
    console.log('VALID COUNT:', valid.length)
}

main()