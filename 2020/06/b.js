const fs = require('fs')
const {intersection} = require('lodash')

const groups = fs.readFileSync('./input.txt', {encoding: 'utf8', flag: 'r'})
    .split('\n\n')
    .map(g => g.split('\n').map(a => a.split('')))
    .map(g => intersection(...g))
    .reduce((acc, g) => acc += g.length, 0)

console.log(groups)