const fs = require('fs')
const {uniq} = require('lodash')

const groups = fs.readFileSync('./input.txt', {encoding: 'utf8', flag: 'r'})
    .split('\n\n')
    .map(g => g.replaceAll('\n', ''))
    .map(g => g.split(''))
    .map(g => uniq(g).length)
    .reduce((acc, g) => acc += g, 0)

console.log(groups)