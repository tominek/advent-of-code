const fs = require('fs')
const {max} = require('lodash')

const passes = fs.readFileSync('./input.txt', {encoding: 'utf8', flag: 'r'}).split('\n')
// const passes = ['BFFFBBFRRR', 'FFFBBBFRRR', 'BBFFBBFRLL']

function createArray(count) {
    const rows = []
    for (let i = 0; i <= count; i++) {
        rows.push(i)
    }

    return rows
}

const ids = passes.reduce((acc, pass) => {
    let rows = createArray(127)
    let columns = createArray(7)
    pass.split('').forEach(char => {
        switch (char) {
            case 'F':
                rows = rows.slice(0, rows.length / 2)
                break
            case 'B':
                rows = rows.slice(rows.length / 2, rows.length)
                break
            case 'L':
                columns = columns.slice(0, columns.length / 2)
                break
            case 'R':
                columns = columns.slice(columns.length / 2, columns.length)
                break
        }
    })
    acc.push(parseInt(rows[0] * 8 + columns[0]))

    return acc
}, [])

console.log('RESULT', max(ids))