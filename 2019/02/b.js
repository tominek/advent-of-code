const inputs = require('./input')

function performAdd (inputs, i) {
  const a = inputs[inputs[i+1]]
  const b = inputs[inputs[i+2]]
  inputs[inputs[i+3]] = a + b
}
function performMultiply (inputs, i) {
  const a = inputs[inputs[i+1]]
  const b = inputs[inputs[i+2]]
  inputs[inputs[i+3]] = a * b
}

function runInputs(noun, verb) {
  const instructions = [ ...inputs ]
  instructions[1] = noun
  instructions[2] = verb
  let terminated = false
  let i = 0
  while (!terminated) {
    const pointer = instructions[i]
    if (pointer === 1) {
      performAdd(instructions, i)
    } else if (pointer === 2) {
      performMultiply(instructions, i)
    } else if (pointer === 99) {
      terminated = true
    }
    i += 4
    // console.log('RESULT', instructions.join(','))
  }
  if (instructions[0] === 19690720) {
    console.log(noun, verb)
  }
}

for (let k = 0; k <= 99; k++) {
  for (let l = 0; l <= 99; l++) {
    runInputs(k, l)
  }
}
