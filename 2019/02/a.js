const input = require('./input')
const Computer = require('../Computer')

const comp = new Computer(input)
comp.run()
console.log(comp.getResult())
// const inputs = [1,9,10,3,2,3,11,0,99,30,40,50]
// const inputs = [2,4,4,5,99,0]
// const inputs = [1,1,1,4,99,5,6,0,99]

// function performAdd (inputs, i) {
//   const a = inputs[inputs[i+1]]
//   const b = inputs[inputs[i+2]]
//   inputs[inputs[i+3]] = a+b
// }
// function performMultiply (inputs, i) {
//   const a = inputs[inputs[i+1]]
//   const b = inputs[inputs[i+2]]
//   inputs[inputs[i+3]] = a*b
// }
//
// let terminated = false
// let i = 0
// while (!terminated){
//   const pointer = inputs[i]
//   if (pointer === 1) {
//     performAdd(inputs, i)
//   } else if (pointer === 2) {
//     performMultiply(inputs, i)
//   } else if (pointer === 99) {
//     terminated = true
//   }
//   i += 4
// }
//
// console.log('RESULT', inputs)
