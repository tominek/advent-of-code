// const fs = require('fs')
//
// fs.open('./getResult.txt', 'w', () => {})
// fs.writeFile('./getResult.txt', '', () => {})

// const wire = require('./inputs')
const wire = {
  first: ['R75','D30','R83','U83','L12','D49','R71','U7','L72'],
  second: ['U62','R66','U55','R34','D71','R55','D58','R83'],
}
// const wire = {
//   first: ['R98','U47','R26','D63','R33','U87','L62','D20','R33','U53','R51'],
//   second: ['U98','R91','D20','R16','D67','R40','U7','R15','U6','R7'],
// }
const regexp = /([RLUD])(\d+)/

let hMax = 0
let hMin = 0
let vMax = 0
let vMin = 0

function findMaxMins(inputs) {
  let h = 0
  let v = 0
  for (let cmd of inputs){
    const match = cmd.match(regexp)
    const dir = match[1]
    const steps = parseInt(match[2])

    if (dir === 'R') {
      h += steps
      if (hMax <= h) {
        hMax = h
      }
    } else if (dir === 'L') {
      h -= steps
      if (hMin >= h) {
        hMin = h
      }
    } else if (dir === 'U') {
      v += steps
      if (vMax <= v) {
        vMax = v
      }
    } else if (dir === 'D') {
      v -= steps
      if (vMin >= v) {
        vMin = v
      }
    }
  }
}

findMaxMins(wire.first)
findMaxMins(wire.second)
console.log('X-SIZE', hMax - hMin)
// console.log(hMax, hMin)
console.log('Y-SIZE', vMax - vMin)
// console.log(vMax, vMin)
const width = hMax - hMin
const height = vMax - vMin

const originX = width - hMax
const originY = height - vMax
console.log('X-ORIGIN', originX)
console.log('Y-ORIGIN', originY)

let circuit = []
let x = originX
let y = originY
initGrid(width, height, circuit)
circuit[originX][originY] = '0'
// printGrid(circuit)

for (let cmd of wire.first){
  const match = cmd.match(regexp)
  const dir = match[1]
  const steps = parseInt(match[2])
  markPath(dir, steps, 'A')
}
x = originX
y = originY
for (let cmd of wire.second){
  const match = cmd.match(regexp)
  const dir = match[1]
  const steps = parseInt(match[2])
  markPath(dir, steps, 'B')
  printGrid(circuit)
}
circuit[originX][originY] = '0'
printGrid(circuit)
findClosestIntersection(circuit)

function markPath (dir, steps, symbol) {
  console.log(dir, steps)
  switch (dir) {
    case 'R':
      for (let r = 0; r <= steps; r++) {
        if (circuit[y][x+r] === symbol) {
          circuit[y][x+r] = '+'
        } else if (circuit[y][x+r] === '░') {
          circuit[y][x+r] = symbol
        } else {
          circuit[y][x+r] = 'X'
        }
      }
      x += steps
      break
    case 'L':
      for (let l = 0; l <= steps; l++) {
        if (circuit[y][x-l] === symbol) {
          circuit[y][x-l] = '+'
        } else if (circuit[y][x-l] === '░') {
          circuit[y][x-l] = symbol
        } else {
          circuit[y][x-l] = 'X'
        }
      }
      x -= steps
      break
    case 'U':
      for (let u = 0; u <= steps; u++) {
        if (circuit[y+u][x] === symbol) {
          circuit[y+u][x] = '+'
        } else if (circuit[y+u][x] === '░') {
          circuit[y+u][x] = symbol
        } else {
          circuit[y+u][x] = 'X'
        }
      }
      y -= steps
      break
    case 'D':
      for (let d = 0; d <= steps; d++) {
        if (circuit[y-d][x] === symbol) {
          circuit[y-d][x] = '+'
        } else if (circuit[y-d][x] === '░') {
          circuit[y-d][x] = symbol
        } else {
          circuit[y-d][x] = 'X'
        }
      }
      y += steps
      break
  }
}

function initGrid(width, height, grid) {
  for (let j = 0; j <= height; j++) {
    grid[j] = []
    for (let i = 0; i <= width; i++) {
      grid[j][i] = '░'
    }
  }
}

function findClosestIntersection(grid) {
  let minDist = 1000
  for (let j = 0; j < grid.length; j++) {
    for (let i = 0; i < grid[0].length; i++) {
      if (grid[j][i] === 'X') {
        const xDist = Math.abs(originX-i)
        const yDist = Math.abs(originY-j)
        if (xDist + yDist < minDist) {
          minDist = xDist + yDist
        }
      }
    }
  }
  console.log('MIN_DIST', minDist)
}

function printGrid (grid) {
  for (const row of grid) {
    // fs.appendFile('./getResult.txt', row.join(''), () => {})
    console.log(row.join(''))
  }
}
