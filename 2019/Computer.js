module.exports = class Computer {

  constructor (input) {
    this.terminated = false
    this.memory = input
  }

  run () {
    let i = 0
    while (!this.terminated) {
      switch (this.memory[i]) {
        case 1:
          this.memory[this.memory[i+3]] = this.memory[this.memory[i+1]] + this.memory[this.memory[i+2]]
          break
        case 2:
          this.memory[this.memory[i+3]] = this.memory[this.memory[i+1]] * this.memory[this.memory[i+2]]
          break
        case 3:
          break
        case 4:
          break
        case 99:
          this.terminated = true
          break
      }
      i += 4
    }
  }

  getResult () {
    return this.memory[0]
  }
}
