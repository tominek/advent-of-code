const min = 138307
const max = 654504

console.log('111111', validPassword(111111))
console.log('223450', validPassword(223450))
console.log('123789', validPassword(123789))

let sum = 0
for (let i = min; i <= max; i++) {
  if (validPassword(i)) {
    sum++
  }
}
console.log('SUM', sum)

function validPassword(number) {
  return hasDoubles(number.toString()) && neverDecrease(number.toString())
}

function hasDoubles (number) {
  for (let i = 0; i < (number.length - 1); i++) {
    if (number[i] === number[i+1]) {
      return true
    }
  }

  return false
}

function neverDecrease (number) {
  for (let i = 0; i < (number.length - 1); i++) {
    if (number[i] > number[i + 1]) {
      return false
    }
  }

  return true
}
