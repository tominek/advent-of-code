const min = 138307
const max = 654504

console.log('112233 - true', validPassword(112233))
console.log('123444 - false', validPassword(123444))
console.log('111122 - true', validPassword(111122))

let sum = 0
for (let i = min; i <= max; i++) {
  if (validPassword(i)) {
    sum++
  }
}
console.log('SUM', sum)

function validPassword(number) {
  return hasDoubles(number.toString()) && neverDecrease(number.toString())
}

function hasDoubles (number) {
  let valid = true
  const triples = number.match(/(\d)\1{2,}/)
  for (let i = 0; i < (number.length - 1); i++) {
    if (number[i] === number[i+1]) {
      return true
    }
  }
  return false
}

function neverDecrease (number) {
  for (let i = 0; i < (number.length - 1); i++) {
    if (number[i] > number[i + 1]) {
      return false
    }
  }
  return true
}
