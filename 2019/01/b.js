const inputs = require('./input')

const test = [ 12, 14, 1969, 100756 ]

function countFuel (mass) {
  return Math.floor(mass / 3) - 2
}

//// TEST
// for (const mass of test) {
//   let fuelTotal = 0
//   let fuel = countFuel(mass)
//   while (fuel > 0) {
//     fuelTotal += fuel
//     fuel = countFuel(fuel)
//   }
//   console.log('FUEL', fuelTotal)
// }

let sum = 0
for (const mass of inputs) {
  let fuelTotal = 0
  let fuel = countFuel(mass)
  while (fuel > 0) {
    fuelTotal += fuel
    fuel = countFuel(fuel)
  }
  sum += fuelTotal
}
console.log('SUM', sum)
