const inputs = require('./input')

const test = [ 12, 14, 1969, 100756 ]

function countFuel (mass) {
  return Math.floor(mass / 3) - 2
}

// TEST
// for (const mass of test) {
//   console.log(countFuel(mass))
// }

let sum = 0
for (const mass of inputs) {
  sum += countFuel(mass)
}
console.log('SUM', sum)
