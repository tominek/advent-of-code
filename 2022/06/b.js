const fs = require('fs')

const CODE_LENGTH = 14

const tests = [
    'mjqjpqmgbljsphdztnvjfqwrcgsmlb',
    'bvwbjplbgvbhsrlpgdmjqwftvncz',
    'nppdvjthqldpwncqszvftbrmjlhg',
    'nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg',
    'zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw',
]

const input = fs.readFileSync('./input.txt', {encoding: 'utf8', flag: 'r'})

const hasNoDuplicities = (array) => (new Set(array)).size === array.length

const findMarkerPosition = (stream) => {
    const buffer = stream.split('')
    const code = []
    for (let i = 0; i < buffer.length; i++) {
        if (code.length >= CODE_LENGTH && hasNoDuplicities(code)) {
            console.log(i, code)
            return
        }
        if (code.length >= CODE_LENGTH) code.shift()
        code.push(buffer[i])
    }
}
tests.forEach(test => findMarkerPosition(test))
findMarkerPosition(input)
