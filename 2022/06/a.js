const fs = require('fs')

const tests = [
    'bvwbjplbgvbhsrlpgdmjqwftvncz',
    'nppdvjthqldpwncqszvftbrmjlhg',
    'nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg',
    'zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw',
]

const input = fs.readFileSync('./input.txt', {encoding: 'utf8', flag: 'r'})

const hasNoDuplicities = (array) => (new Set(array)).size === array.length

const findMarkerPosition = (stream) => {
    const buffer = stream.split('')
    const code = []
    for (let i = 0; i < buffer.length; i++) {
        if (code.length > 3 && hasNoDuplicities(code)) {
            console.log(i, code)
            return
        }
        // console.log({code})
        if (code.length >= 4) code.shift()
        code.push(buffer[i])
    }
}
// tests.forEach(test => findMarkerPosition(test))
findMarkerPosition(input)
