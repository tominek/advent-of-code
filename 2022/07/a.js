const fs = require('fs')

const changeDirectoryCommand = /\$ cd (?<path>.*)/

let currentDirectory = '/'
const directories = fs.readFileSync('./test.txt', {encoding: 'utf8', flag: 'r'})
    .split('\n')
    .reduce((dirs, line) => {
        const cdCommandResult = line.match(changeDirectoryCommand)
        if (cdCommandResult) {
            const path = cdCommandResult.groups.path
            if (path === '..') {
                // Navigate back
            } else if (path !== '/') {
                currentDirectory.concat(`${path}/`)
            }
        }

        if (!dirs.find(dir => dir.path === currentDirectory)) {
            
        }

        return dirs
    }, [])

