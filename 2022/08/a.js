const fs = require('fs')

const forest = fs.readFileSync('./input.txt', {encoding: 'utf8', flag: 'r'})
    .split('\n')
    .reduce((forest, line) => {
        if (line.length > 0) {
            forest.push([...line.split('')])
        }
        return forest
    }, [])

const visibilityMap = JSON.parse(JSON.stringify(forest))

const height = forest.length
const width = forest[0].length

const isVisible = (x, y) => {
    let topVisible = true
    let leftVisible = true
    let bottomVisible = true
    let rightVisible = true

    // TOP
    for (let yPos = y - 1; yPos >= 0; yPos--) {
        if (forest[y][x] <= forest[yPos][x]) topVisible = false
    }
    // LEFT
    for (let xPos = x - 1; xPos >= 0; xPos--) {
        if (forest[y][x] <= forest[y][xPos]) leftVisible = false
    }
    // BOTTOM
    for (let yPos = y + 1; yPos < height; yPos++) {
        if (forest[y][x] <= forest[yPos][x]) bottomVisible = false
    }
    // RIGHT
    for (let xPos = x + 1; xPos < width; xPos++) {
        if (forest[y][x] <= forest[y][xPos]) rightVisible = false
    }

    return topVisible || leftVisible || bottomVisible || rightVisible
}

let visibleCount = 0
for (let y = 0; y < height; y++) {
    for (let x = 0; x < width; x++) {
        if (
            y === 0
            || y === height - 1
            || x === 0
            || x === width - 1
            || isVisible(x, y)
        ) {
            visibilityMap[y][x] = 'V'
            visibleCount++
        } else {
            visibilityMap[y][x] = '_'
        }
    }
}

// visibilityMap.forEach(line => console.log(line.join('')))
console.log({visibleCount})
