const fs = require('fs')

const forest = fs.readFileSync('./input.txt', {encoding: 'utf8', flag: 'r'})
    .split('\n')
    .reduce((forest, line) => {
        if (line.length > 0) {
            forest.push([...line.split('').map(h => Number(h))])
        }
        return forest
    }, [])

// forest.forEach(line => console.log(line.join('')))

const height = forest.length
const width = forest[0].length
let maxScenicScore = 0
let bestPosition = {x: 0, y :0, height: 0}

const countScenicScore = (x, y) => {
    let topScore = 0
    let leftScore = 0
    let bottomScore = 0
    let rightScore = 0

    // TOP
    for (let yPos = y - 1; yPos >= 0; yPos--) {
        topScore++
        if (forest[y][x] <= forest[yPos][x]) break
    }
    // LEFT
    for (let xPos = x - 1; xPos >= 0; xPos--) {
        leftScore++
        if (forest[y][x] <= forest[y][xPos]) break
    }
    // BOTTOM
    for (let yPos = y + 1; yPos < height; yPos++) {
        bottomScore++
        if (forest[y][x] <= forest[yPos][x]) break
    }
    // RIGHT
    for (let xPos = x + 1; xPos < width; xPos++) {
        rightScore++
        if (forest[y][x] <= forest[y][xPos]) break
    }
    const score = topScore * leftScore * bottomScore * rightScore
    if (score > maxScenicScore) {
        maxScenicScore = score
        bestPosition.x = x
        bestPosition.y = y
        bestPosition.height = forest[y][x]
        // console.log({x,y,height,topScore, leftScore, bottomScore, rightScore})
    }
}

for (let y = 0; y < height; y++) {
    for (let x = 0; x < width; x++) {
        if (
            !(y === 0
                || y === height - 1
                || x === 0
                || x === width - 1)
        ) {
            countScenicScore(x, y)
        }
    }
}

console.log({maxScenicScore, bestPosition})
