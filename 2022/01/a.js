const fs = require('fs')

let i = 0
const elves = fs.readFileSync('./input.txt', {encoding: 'utf8', flag: 'r'})
    .split('\n')
    .reduce((acc, current) => {
        if (!acc[i]) {
            acc.push(0)
        }
        if(current.length > 0) {
            acc[i] += Number(current)
        } else {
            i++
        }
        return acc
    }, [])
    .sort((a, b) => b - a)

// console.log({elves})
console.log({
    maxElf: elves[0]
})
