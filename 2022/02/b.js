const fs = require('fs')

const resolveMatch = ([opponent, result]) => {
    if (result === 'LOSE' && opponent === 'ROCK') return 0 + 3
    if (result === 'DRAW' && opponent === 'ROCK') return 3 + 1
    if (result === 'WIN' && opponent === 'ROCK') return 6 + 2
    if (result === 'LOSE' && opponent === 'PAPER') return 0 + 1
    if (result === 'DRAW' && opponent === 'PAPER') return 3 + 2
    if (result === 'WIN' && opponent === 'PAPER') return 6 + 3
    if (result === 'LOSE' && opponent === 'SCISSORS') return 0 + 2
    if (result === 'DRAW' && opponent === 'SCISSORS') return 3 + 3
    if (result === 'WIN' && opponent === 'SCISSORS') return 6 + 1
    return 0
}

const score = fs.readFileSync('./input-b.txt', {encoding: 'utf8', flag: 'r'})
    .split('\n')
    .reduce((acc, match) => acc + resolveMatch(match.split(' ')), 0)

console.log({score})
