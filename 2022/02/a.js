const fs = require('fs')

const resolveMatch = ([opponent, me]) => {
    if (me === 'ROCK' && opponent === 'ROCK') return 3 + 1
    if (me === 'PAPER' && opponent === 'ROCK') return 6 + 2
    if (me === 'SCISSORS' && opponent === 'ROCK') return 0 + 3
    if (me === 'ROCK' && opponent === 'PAPER') return 0 + 1
    if (me === 'PAPER' && opponent === 'PAPER') return 3 + 2
    if (me === 'SCISSORS' && opponent === 'PAPER') return 6 + 3
    if (me === 'ROCK' && opponent === 'SCISSORS') return 6 + 1
    if (me === 'PAPER' && opponent === 'SCISSORS') return 0 + 2
    if (me === 'SCISSORS' && opponent === 'SCISSORS') return 3 + 3
    return 0
}

const score = fs.readFileSync('./input.txt', {encoding: 'utf8', flag: 'r'})
    .split('\n')
    .reduce((acc, match) => acc + resolveMatch(match.split(' ')), 0)

console.log({score})
