const fs = require('fs')

const regex = /(?<a>\d+)-(?<b>\d+),(?<x>\d+)-(?<y>\d+)/

const isContained = (a, b, x, y) => (a <= x && b >= y) || (x <= a && y >= b)

const sum = fs.readFileSync('./input.txt', {encoding: 'utf8', flag: 'r'})
    .split('\n')
    .reduce((sum, line) => {
        if (line.length > 0) {
            const {a, b, x, y} = line.match(regex).groups
            if (isContained(Number(a), Number(b), Number(x), Number(y))) {
                sum++
            }
        }
        return sum
    }, 0)

console.log({sum})
