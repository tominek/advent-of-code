class Monkey {
    id
    items
    inspect
    getThrowTarget
    inspectionCount
}

const testMonkeys = [
    {
        id: 0,
        items: [79, 98],
        inspect: (value) => value * 19,
        getThrowTarget: (value) => value % 23 === 0 ? 2 : 3,
        inspectionCount: 0,
    },
    {
        id: 1,
        items: [54, 65, 75, 74],
        inspect: (value) => value + 6,
        getThrowTarget: (value) => value % 19 === 0 ? 2 : 0,
        inspectionCount: 0,
    },
    {
        id: 2,
        items: [79, 60, 97],
        inspect: (value) => value * value,
        getThrowTarget: (value) => value % 13 === 0 ? 1 : 3,
        inspectionCount: 0,
    },
    {
        id: 3,
        items: [74],
        inspect: (value) => value + 3,
        getThrowTarget: (value) => value % 17 === 0 ? 0 : 1,
        inspectionCount: 0,
    },
]

const mainMonkeys = [
    {
        id: 0,
        items: [63, 57],
        inspect: (value) => value * 11,
        getThrowTarget: (value) => value % 7 === 0 ? 6 : 2,
        inspectionCount: 0,
    },
    {
        id: 1,
        items: [82, 66, 87, 78, 77, 92, 83],
        inspect: (value) => value + 1,
        getThrowTarget: (value) => value % 11 === 0 ? 5 : 0,
        inspectionCount: 0,
    },
    {
        id: 2,
        items: [97, 53, 53, 85, 58, 54],
        inspect: (value) => value * 7,
        getThrowTarget: (value) => value % 13 === 0 ? 4 : 3,
        inspectionCount: 0,
    },
    {
        id: 3,
        items: [50],
        inspect: (value) => value + 3,
        getThrowTarget: (value) => value % 3 === 0 ? 1 : 7,
        inspectionCount: 0,
    },
    {
        id: 4,
        items: [64, 69, 52, 65, 73],
        inspect: (value) => value + 6,
        getThrowTarget: (value) => value % 17 === 0 ? 3 : 7,
        inspectionCount: 0,
    },
    {
        id: 5,
        items: [57, 91, 65],
        inspect: (value) => value + 5,
        getThrowTarget: (value) => value % 2 === 0 ? 0 : 6,
        inspectionCount: 0,
    },
    {
        id: 6,
        items: [67, 91, 84, 78, 60, 69, 99, 83],
        inspect: (value) => value * value,
        getThrowTarget: (value) => value % 5 === 0 ? 2 : 4,
        inspectionCount: 0,
    },
    {
        id: 7,
        items: [58, 78, 69, 65],
        inspect: (value) => value + 7,
        getThrowTarget: (value) => value % 19 === 0 ? 5 : 1,
        inspectionCount: 0,
    },
]

const monkeys = testMonkeys
for (let i = 0; i < 1000; i++) {
    for (const monkey of monkeys) {
        for (let index = 0; index < monkey.items.length; index++) {
            const inspectedItem = monkey.inspect(monkey.items[index])
            monkey.inspectionCount++

            const targetMonkey = monkeys.find(m => m.id === monkey.getThrowTarget(inspectedItem))
            // console.log({item: monkey.items[index], inspectedItem, targetMonkey: targetMonkey.id})
            targetMonkey.items.push(inspectedItem)
        }
        monkey.items = []
    }
}

console.log({monkeys: monkeys
        // .sort((a, b) => b.inspectionCount - a.inspectionCount)
        // .slice(0, 2)
})

const result = monkeys
    .sort((a, b) => b.inspectionCount - a.inspectionCount)
    .slice(0, 2)
    .reduce((sum, monkey) => sum * monkey.inspectionCount, 1)

console.log({result})
