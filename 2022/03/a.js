const fs = require('fs')

const priority = ['', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

const findPriority = (first, second) => {
    let shared = []
    for (const char of first.split('')) {
        if (second.includes(char)) {
            if (!shared.includes(char)) {
                shared.push(char)
            }
        }
    }
    console.log({first, second, shared})
    return shared.reduce((sum, char) => sum + priority.findIndex(p => p === char), 0)
}

const sum = fs.readFileSync('./input.txt', {encoding: 'utf8', flag: 'r'})
    .split('\n')
    .reduce((sum, line) => sum + findPriority(
        line.substring(0, line.length / 2),
        line.substring(line.length / 2)
    ), 0)

console.log({sum})
