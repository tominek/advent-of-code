const fs = require('fs')

const priority = ['', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

const findPriority = (first, second, third) => {
    let shared = []
    for (const char of first.split('')) {
        if (second.includes(char) && third.includes(char)) {
            if (!shared.includes(char)) {
                shared.push(char)
            }
        }
    }
    // console.log({first, second, third, shared})
    return shared.reduce((sum, char) => sum + priority.findIndex(p => p === char), 0)
}

const sum = fs.readFileSync('./input.txt', {encoding: 'utf8', flag: 'r'})
    .split('\n')
    .reduce((acc, line) => {
        // console.log({acc, line})
        if (acc.length <= 0) {
            acc.push([])
        }

        const last = acc.at(-1)
        if (last.length < 3) {
            last.push(line)
        } else {
            acc.push([line])
        }

        return acc
    }, [])
    .reduce((sum, trio) => sum + findPriority(...trio), 0)

console.log({sum})
