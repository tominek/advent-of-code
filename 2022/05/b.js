const fs = require('fs')

const regex = /^move (?<count>\d+) from (?<from>\d+) to (?<to>\d+)/

const test = {
    1: ['Z', 'N'], 2: ['M', 'C', 'D'], 3: ['P'],
}

const input = {
    1: ['B', 'S', 'V', 'Z', 'G', 'P', 'W'],
    2: ['J', 'V', 'B', 'C', 'Z', 'F'],
    3: ['V', 'L', 'M', 'H', 'N', 'Z', 'D', 'C'],
    4: ['L', 'D', 'M', 'Z', 'P', 'F', 'J', 'B'],
    5: ['V', 'F', 'C', 'G', 'J', 'B', 'Q', 'H'],
    6: ['G', 'F', 'Q', 'T', 'S', 'L', 'B'],
    7: ['L', 'G', 'C', 'Z', 'V'],
    8: ['N', 'L', 'G'],
    9: ['J', 'F', 'H', 'C'],
}

const move = (stacks, count, from, to) => {
    const crates = stacks[from].splice(-count)
    stacks[to].push(...crates)

    return stacks
}

const stacks = fs.readFileSync('./input.txt', {encoding: 'utf8', flag: 'r'})
    .split('\n')
    .reduce((stacks, line) => {
        if (line.length > 0) {
            const {count, from, to} = line.match(regex).groups

            return move(stacks, Number(count), Number(from), Number(to))
        }
        return stacks
    }, input)

console.log({top: Object.values(stacks).reduce((top, s) => top + s.at(-1), '')})
Object.values(stacks).forEach((s, index) => console.log(index + 1, s))
// console.log({stacks})
