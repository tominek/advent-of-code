<?php

namespace Day06;

class First
{

    private $history = [];

    private function addToHistory($currentState)
    {
        if ($this->notInHistory($currentState)) {
            $this->history[] = implode("-", $currentState);
            return true;
        }
        return false;
    }

    private function notInHistory($currentState)
    {
        $item = implode("-", $currentState);
        for ($i = 0; $i < count($this->history); $i++) {
            if ($this->history[$i] == $item) {
                echo $i . "\n";
                return false;
            }
        }
        return true;

//        return !in_array($item, $this->history);
    }

    private function getMaxPosition(array $banks)
    {
        $max = max($banks);
        for ($i = 0; $i < count($banks); $i++) {
            if ($banks[$i] == $max) {
//                echo "Max value " . $max . " on position " . $i . "\n";
                return $i;
            }
        }
        die;
    }

    public function balance(array $currentState)
    {
        $cycles = 0;
        while ($this->addToHistory($currentState)) {
            $position = $this->getMaxPosition($currentState);
//            echo implode("-", $currentState) . "\n";

            $blocks = $currentState[$position];
            $currentState[$position] = 0;
            $rPos = $position;
            while ($blocks > 0) {
                $rPos++;
                if ($rPos == count($currentState)) $rPos = 0;
                $currentState[$rPos]++;
                $blocks--;
            }

//            $this->addToHistory($currentState);
            $cycles++;
//            echo "Cycle " . $cycles . "\n";
        }

        return $cycles;
    }
}

$example = [0, 2, 7, 0];
$test = [14, 0, 15, 12, 11, 11, 3, 5, 1, 6, 8, 4, 9, 1, 8, 4];

echo (new First())->balance($test);
