<?php

namespace Day04;

class First
{
    private function isPassphraseValid(string $passphrase)
    {
        $words = explode(" ", $passphrase);
        for ($i = 0; $i < count($words) - 1; $i++) {
            for ($j = $i + 1; $j < count($words); $j++)
                if ($words[$i] == $words[$j]) {
                    return false;
                }
        }
        return true;
    }

    public function validPassPhrasesCount(string $path)
    {
        $sum = 0;
        $handle = fopen($path, "r");
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                if ($this->isPassphraseValid(trim($line))) {
                    $sum++;
                }
            }
            fclose($handle);
        }

        return $sum;
    }
}

echo (new First())->validPassPhrasesCount("test");