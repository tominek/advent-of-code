<?php

namespace Day04;

class Second
{
    private function isPassphraseValid(string $passphrase)
    {
        $words = explode(" ", $passphrase);
        for ($i = 0; $i < count($words) - 1; $i++) {
            for ($j = $i + 1; $j < count($words); $j++) {

                $word1 = $words[$i];
                $word2 = $words[$j];

                $differences = array_diff(str_split($word1), str_split($word2));
                $differences2 = array_diff(str_split($word2), str_split($word1));

                if (empty($differences) && empty($differences2)) {
                    echo $words[$i] . " and " . $words[$j] . "\n";
                    return false;
                }
            }
        }
        return true;
    }

    public function validPassPhrasesCount(string $path)
    {
        $sum = 0;
        $handle = fopen($path, "r");
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                if ($this->isPassphraseValid(trim($line))) {
                    $sum++;
                }
            }
            fclose($handle);
        }
        return $sum;
    }
}

echo (new Second())->validPassPhrasesCount("test");