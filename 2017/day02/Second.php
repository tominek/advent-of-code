<?php

namespace Day02;

class Second
{
    public function getSum(string $path)
    {
        $sum = 0;

        $handle = fopen($path, "r");
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                $numbers = explode(" ", trim($line));
                foreach ($numbers as $number) {
                    foreach ($numbers as $divider) {
                        if ($number > $divider) {
//                            echo $number . " > " . $divider . "\n";
                            if ($number % $divider == 0) $sum += $number / $divider;
                        }
                    }
//                    echo "--------\n";
                }
            }

            fclose($handle);
        }
        return $sum;
    }
}

echo "Sum is " . (new Second())->getSum("test");