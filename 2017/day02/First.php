<?php

namespace Day02;

class First
{
    public function getSum(string $path)
    {
        $sum = 0;

        $handle = fopen($path, "r");
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                $numbers = explode(" ", trim($line));
                $min = min($numbers);
                $max = max($numbers);
                $sum += $max - $min;
            }

            fclose($handle);
        }
        return $sum;
    }
}

echo "Sum is " . (new First())->getSum("test");