<?php
/**
 * Created by PhpStorm.
 * User: KNEZET01
 * Date: 11/12/2015
 * Time: 15:20
 */

function vowels($word)
{
    $count  = 0;
    $vowels = ["a", "e", "i", "o", "u"];
    foreach ($vowels as $vowel) {
        $count += substr_count($word, $vowel);
    }
    return $count >= 3;
}

function double($word)
{
    $chars = str_split($word);
    for ($i = 1; $i < count($chars); $i++) {
        if ($chars[$i - 1] == $chars[$i]) {
            return true;
        }
    }
    return false;
}

function forbidden($word)
{
    $forbidden = ["ab", "cd", "pq", "xy"];
    foreach ($forbidden as $forb) {
        if (strpos($word, $forb) !== false) {
            return false;
        }
    }
    return true;
}

function doubles($word)
{
    $regex = "/(\\w{2}).*(\\1)/";
    return preg_match($regex, $word);
}

function palindrom($word)
{
    $regex = "/(\\w)\\w(\\1)/";
    return preg_match($regex, $word);
}

$list = fopen("res/5.txt", "r");
$sum  = 0;
$sum1 = 0;

while (!feof($list)) {
    $word = trim(fgets($list));

    if (vowels($word) && double($word) && forbidden($word)) {
        $sum++;
    }

    if (palindrom($word) && doubles($word)) {
        $sum1++;
    }
}
fclose($list);

print $sum . "\n";
print $sum1;