<?php
/**
 * Created by PhpStorm.
 * User: KNEZET01
 * Date: 11/12/2015
 * Time: 09:59
 */

$list   = fopen("res/2.txt", "r");
$paper  = 0;
$ribbon = 0;

while (!feof($list)) {
    $dim = explode("x", fgets($list));

    $a     = $dim[0] * $dim[1];
    $b     = $dim[1] * $dim[2];
    $c     = $dim[2] * $dim[0];
    $extra = min([$a, $b, $c]);
    $paper += 2 * ($a + $b + $c) + $extra;

    $x   = 2 * ($dim[0] + $dim[1]);
    $y   = 2 * ($dim[1] + $dim[2]);
    $z   = 2 * ($dim[2] + $dim[0]);
    $bow = min([$x, $y, $z]);
    $ribbon += $dim[0] * $dim[1] * $dim[2] + $bow;
}
fclose($list);

print $ribbon . "\n";
print $paper;