<?php
/**
 * Created by PhpStorm.
 * User: KNEZET01
 * Date: 17/12/2015
 * Time: 16:55
 */

$number = "1";
$result = "";

for ($i = 0; $i < 5; $i++) {
    $digits   = str_split($number);
    $previous = $digits[0];
    $groups   = "";
    if (count($digits) > 1) {
        for ($i = 1; $i < count($digits); $i++) {
            if ($digits[$i] == $previous) {
                $groups .= $digits[$i];
            }
        }
    } else {
        $groups .= $previous;
    }
    $result .= strlen($groups) . $groups[0];
}

print $result;