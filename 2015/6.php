<?php
/**
 * Created by PhpStorm.
 * User: KNEZET01
 * Date: 11/12/2015
 * Time: 16:41
 */

function printGrid($grid)
{
    file_put_contents("6_tmp.txt", "");
    for ($i = 0; $i < 1000; $i++) {
        for ($j = 0; $j < 1000; $j++) {
            file_put_contents("6_tmp.txt", ($grid[$i][$j] ? "۞" : "-"), FILE_APPEND);
        }
        file_put_contents("6_tmp.txt", "\n", FILE_APPEND);
    }
}

$regex = "/(turn on|turn off|toggle) (\\d*),(\\d*) through (\\d*),(\\d*)/";

$grid = null;
for ($i = 0; $i < 1000; $i++) {
    for ($j = 0; $j < 1000; $j++) {
        $grid[$i][$j] = 0;
    }
}

$instructions = fopen("res/6.txt", "r");

while (!feof($instructions)) {
    $instruction = fgets($instructions);
    preg_match($regex, $instruction, $data);
//    print $data[1] . " | " . $data[2] . " | " . $data[3] . " | " . $data[4] . " | " . $data[5] . "\n";
    for ($i = $data[2]; $i <= $data[4]; $i++) {
        for ($j = $data[3]; $j <= $data[5]; $j++) {
            switch ($data[1]) {
                case "turn on":
                    $grid[$i][$j]++;
                    break;
                case "turn off":
                    if ($grid[$i][$j] > 0) $grid[$i][$j]--;
                    break;
                case "toggle":
                    $grid[$i][$j] = $grid[$i][$j] + 2;
                    break;
                default:
                    throw new Exception("This should not happen.");
            }
        }
    }
}
fclose($instructions);

$sum = 0;
for ($i = 0; $i < 1000; $i++) {
    for ($j = 0; $j < 1000; $j++) {
        $sum += $grid[$i][$j];
    }
}
print $sum;