<?php
/**
 * Created by PhpStorm.
 * User: KNEZET01
 * Date: 14/12/2015
 * Time: 17:24
 */

function to16bit($number)
{
    return sprintf("%016d", decbin($number));
}

$reg0 = "/(\\w+) (AND|NAND|OR|RSHIFT|LSHIFT) (\\w+) -> (\\w+)/";
$reg1 = "/(\\D+) (AND|NAND|OR|RSHIFT|LSHIFT) (\\D+) -> (\\w+)/";
$reg2 = "/^(\\d+) -> (\\w+)/m";

$file = fopen("res/7.txt", "r");
while (!feof($file)) {
    $line = fgets($file);

    if (preg_match($reg2, $line)) {
        print trim($line) . "\n";
    }
}