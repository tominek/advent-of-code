<?php
/**
 * Created by PhpStorm.
 * User: KNEZET01
 * Date: 15/12/2015
 * Time: 16:28
 */

$chars     = 0;
$realChars = 0;

$file = fopen("res/8.txt", "r");
while (!feof($file)) {
    $line = trim(fgets($file));

    $chars += strlen($line);

    $tmp  = trim($line);
    $tmp  = substr($tmp, 1);
    $tmp  = substr($tmp, 0, -1);
    $tmp1 = preg_replace('/(\\\\x[0-9A-Fa-f]{2})/', "X", $tmp);
    $tmp2 = preg_replace('/(\\\")/', "\"", $tmp1);
    $tmp2 = preg_replace('/(\\\\\\\\)/', '\\', $tmp2);
    $realChars += strlen($tmp2);

    print "Step 1: " . trim($line) . "\n";
    print "Step 2: " . $tmp . "\n";
    print "Step 3: " . $tmp1 . "\n";
    print "Step 4: " . $tmp2 . "\n";
    print "\n";
}

print "======================\n";
print $chars . "\n";
print $realChars . "\n";
print $chars - $realChars;