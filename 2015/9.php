<?php
/**
 * Created by PhpStorm.
 * User: KNEZET01
 * Date: 17/12/2015
 * Time: 16:00
 */

function findShortest($distances, $galaxies, $sum)
{
    $dist = array();
    if (count($galaxies) >= 1) {
        print $sum . "\n";
    }
    for ($i = 1; $i < count($galaxies); $i++) {
        $dist[] = $distances[$galaxies[0]][$galaxies[$i]];
    }
    $sum          = min($dist);
    $galaxiesRest = $galaxies;
    unset($galaxiesRest[0]);
    findShortest($distances, $galaxiesRest, $sum);
}

$galaxies = ["London", "Dublin", "Belkfast"];
//$galaxies  = ["Tristram", "AlphaCentauri", "Snowdin", "Tambi", "Faerun", "Norrath", "Straylight"];
$distances = array();
$sum       = 0;

$file = fopen("res/9-1.txt", "r");
while (!feof($file)) {
    $line = trim(fgets($file));
    preg_match("/(\\w+) to (\\w+) = (\\d+)/", $line, $match);

    $distances[$match[0]][$match[1]] = $match[2];
}

findShortest($distances, $galaxies, $sum);