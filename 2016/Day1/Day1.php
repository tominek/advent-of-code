<?php

class Day1
{
    private $test = ['R4', 'R3', 'L3', 'L2', 'L1', 'R1', 'L1', 'R2', 'R3', 'L5', 'L5', 'R4', 'L4', 'R2', 'R4', 'L3', 'R3', 'L3', 'R3', 'R4', 'R2', 'L1', 'R2', 'L3', 'L2', 'L1', 'R3', 'R5', 'L1', 'L4', 'R2', 'L4', 'R3', 'R1', 'R2', 'L5', 'R2', 'L189', 'R5', 'L5', 'R52', 'R3', 'L1', 'R4', 'R5', 'R1', 'R4', 'L1', 'L3', 'R2', 'L2', 'L3', 'R4', 'R3', 'L2', 'L5', 'R4', 'R5', 'L2', 'R2', 'L1', 'L3', 'R3', 'L4', 'R4', 'R5', 'L1', 'L1', 'R3', 'L5', 'L2', 'R76', 'R2', 'R2', 'L1', 'L3', 'R189', 'L3', 'L4', 'L1', 'L3', 'R5', 'R4', 'L1', 'R1', 'L1', 'L1', 'R2', 'L4', 'R2', 'L5', 'L5', 'L5', 'R2', 'L4', 'L5', 'R4', 'R4', 'R5', 'L5', 'R3', 'L1', 'L3', 'L1', 'L1', 'L3', 'L4', 'R5', 'L3', 'R5', 'R3', 'R3', 'L5', 'L5', 'R3', 'R4', 'L3', 'R3', 'R1', 'R3', 'R2', 'R2', 'L1', 'R1', 'L3', 'L3', 'L3', 'L1', 'R2', 'L1', 'R4', 'R4', 'L1', 'L1', 'R3', 'R3', 'R4', 'R1', 'L5', 'L2', 'R2', 'R3', 'R2', 'L3', 'R4', 'L5', 'R1', 'R4', 'R5', 'R4', 'L4', 'R1', 'L3', 'R1', 'R3', 'L2', 'L3', 'R1', 'L2', 'R3', 'L3', 'L1', 'L3', 'R4', 'L4', 'L5', 'R3', 'R5', 'R4', 'R1', 'L2', 'R3', 'R5', 'L5', 'L4', 'L1', 'L1'];
//    private $test = ['R8', 'R4', 'R4', 'R8'];
//    private $test = ['R5', 'L5', 'R5', 'R3'];
//    private $test = ['R2', 'R2', 'R2'];
//    private $test = ['R2', 'L3'];
    private $directions = [0 => "N", 1 => "E", 2 => "S", 3 => "W"];
    private $direction = 0;
    private $Y = 0;
    private $X = 0;

    private $grid = [];
    private $intersection = false;
    private $intX = 0;
    private $intY = 0;

    private $image;
    private $imageX = 500;
    private $imageY = 500;

    public function run()
    {
//        if (file_exists("coordination.txt")) {
//            unlink("coordination.txt");
//        }
        $this->initImage();
        $this->grid[0][0] = true;

        foreach ($this->test as $instruction) {
            preg_match('/([RL])(\d+)/', $instruction, $matches);
            $turn = $matches[1];
            $distance = $matches[2];

            $this->changeDirection($turn);
            $this->computeCoordination($distance);
        }

        $this->printInfo();
        $this->saveImage();
    }

    private function changeDirection($turn)
    {
        if ($turn == "R") {
            if ($this->direction == 3) {
                $this->direction = 0;
            } else $this->direction += 1;
        } else {
            if ($this->direction == 0) {
                $this->direction = 3;
            } else $this->direction -= 1;
        }
    }

    private function computeCoordination($distance)
    {
        $incX = 0;
        $incY = 0;
        switch ($this->direction) {
            case 0:
                $incY = 1;
                break;
            case 1:
                $incX = 1;
                break;
            case 2:
                $incY = -1;
                break;
            case 3:
                $incX = -1;
                break;
            default:
                throw new \Exception("This should not happen.");
        }
        for ($i = 1; $i <= $distance; $i++) {
            $this->X += $incX;
            $this->Y += $incY;
            $this->markCell();
//            file_put_contents("coordination.txt", "\n $this->X $this->Y", FILE_APPEND);
        }
    }

    private function markCell()
    {
        $intersection = (isset($this->grid[$this->X][$this->Y]) && $this->grid[$this->X][$this->Y] == true);
        if (!$this->intersection) {
            if ($intersection) {
                $this->intX = $this->X;
                $this->intY = $this->Y;
                $this->intersection = true;
            }
        }
        $this->paintActualCell($intersection);
        $this->grid[$this->X][$this->Y] = true;
    }

    private function paintActualCell($interception = false)
    {
        if ($interception) {
            $color = imagecolorallocate($this->image, 255, 0, 0);
        } else {
            $color = imagecolorallocate($this->image, 255, 255, 255);
        }
        imagesetpixel($this->image, $this->X + ($this->imageX / 2), $this->Y + ($this->imageY / 2), $color);
    }

    private function printInfo()
    {
        echo "\nX :" . $this->X;
        echo "\nY :" . $this->Y;
        echo "\nDistance :" . (abs($this->X) + abs($this->Y));

        echo "\n\nX :" . $this->intX;
        echo "\nY :" . $this->intY;
        echo "\nIntersection Distance :" . (abs($this->intX) + abs($this->intY));
    }

    private function saveImage()
    {
        imageflip($this->image, IMG_FLIP_VERTICAL);
        imagepng($this->image, "route.png");
        imagedestroy($this->image);
    }

    private function initImage()
    {
        $this->image = imagecreatetruecolor($this->imageX, $this->imageY);
        $this->paintActualCell(true);
    }
}

(new Day1())->run();