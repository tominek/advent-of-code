<?php

class Day6
{
    public function run()
    {
        $word = [];

        $messages = $this->getInput();
        foreach ($messages as $message) {
            $letters = str_split($message);
            for ($i = 0; $i < 8; $i++) {
                if (isset($word[$i][$letters[$i]])) {
                    $word[$i][$letters[$i]]++;
                } else {
                    $word[$i][$letters[$i]] = 1;
                }
            }
        }
        var_dump($word);
//        for ($i = 0; $i < 8; $i++) {
//            echo max($word[$i]);
//        }
    }

    private function getInput()
    {
        $messages = [];
        $test = fopen("test", "r");
        while (!feof($test)) {
            $messages[] = fgets($test);
        }
        return $messages;
    }
}

(new Day6())->run();