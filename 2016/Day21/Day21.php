<?php

class Day21
{
    private $password = 'abcdefg';

    public function run()
    {

    }

    public function swapPositions($position1, $position2)
    {
        $password = str_split($this->password);
        $letter1 = $password[$position1];
        $letter2 = $password[$position2];

        $password[$position1] = $letter2;
        $password[$position2] = $letter1;

        $this->password = implode('', $password);
    }

    public function swapLetters($letter1, $letter2)
    {
        $position1 = strpos($this->password, $letter1);
        $position2 = strpos($this->password, $letter2);
        $this->swapPositions($position1, $position2);
    }

    public function rotate($direction, $steps)
    {
        $password = str_split($this->password);
        if ($direction == 'right') {
            $steps = count($password) - $steps;
        }

        for ($i = 0; $i < $steps; $i++) {
            array_push($password, array_shift($password));
        }
        $this->password = implode('', $password);
    }

    public function rotateBasedOnPositionOf($letter)
    {
        $position = strpos($this->password, $letter);
        if ($position >= 4) {
            $steps = $position + 2;
        } else {
            $steps = $position + 1;
        }
        $this->rotate('right', $steps);
    }

    public function reversePositions($from, $to)
    {
        $password = str_split($this->password);
        $subArray = [];
        for ($i = $from; $i <= $to; $i++) {
            $subArray[$i] = $password[$i];
        }
        $reversed = array_reverse($subArray);
        for ($i = 0; $i < count($reversed); $i++) {
            $password[$from + $i] = $reversed[$i];
        }
        $this->password = implode('', $password);
    }

    public function movePosition($position1, $position2)
    {
        $password = str_split($this->password);
        $moved = array_splice($password, $position1, 1);
        array_splice($password, $position2, 0, $moved);

        $this->password = implode('', $password);
    }

    public function getPassword()
    {
        return $this->password;
    }
}

//(new Day21())->run();
$day = new Day21();
$day->movePosition(1, 0);
echo $day->getPassword();