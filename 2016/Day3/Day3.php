<?php

class Day3
{
    private $sum = 0;

    public function run()
    {
        $lines = $this->getInput();
        foreach ($lines as $line) {
            $first = $line[0];
            $second = $line[1];
            $third = $line[2];

            $this->checkTriangle($first, $second, $third);
        }
        echo $this->sum;
    }

    private function checkTriangle($first, $second, $third)
    {
        if (
            ($first + $second) > $third
            && ($first + $third) > $second
            && ($second + $third) > $first
        ) {
            $this->sum++;
        }
    }

    private function getInput()
    {
        $instructions = [];

        $first = [];
        $second = [];
        $third = [];
        $test = fopen("test", "r");
        while (!feof($test)) {
            $line = fgets($test);
            preg_match('/(\d+)\s+(\d+)\s+(\d+)/', $line, $matches);

            $first[] = $matches[1];
            $second[] = $matches[2];
            $third[] = $matches[3];

            if (count($third) == 3) {
                $instructions[] = $first;
                $instructions[] = $second;
                $instructions[] = $third;

                $first = [];
                $second = [];
                $third = [];
            }
        }
        return $instructions;
    }
}

(new Day3())->run();