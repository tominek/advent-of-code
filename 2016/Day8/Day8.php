<?php

class Day8
{
    private $displayX = 50;
    private $displayY = 6;

    private function getInput()
    {
        $messages = [];
        $test = fopen("test", "r");
        while (!feof($test)) {
            $messages[] = fgets($test);
        }
        return $messages;
    }

    private $display = [];

    public function run()
    {
        $this->prepareDisplay();
        foreach ($this->getInput() as $instruction) {
            $this->processInstruction($instruction);
        }
        $this->printDisplay();
        $this->countOnPixels();
    }

    private function processInstruction($instruction)
    {
        if (preg_match('/rect (\d+)x(\d+)/', $instruction, $matches)) {
            $a = $matches[1];
            $b = $matches[2];
            $this->rectangle($a, $b);
        } else if (preg_match('/rotate (row|column) (x|y)=(\d+) by (\d+)/', $instruction, $matches)) {
            $direction = $matches[1];
            $position = $matches[3];
            $value = $matches[4];
            $this->rotate($direction, $position, $value);
        } else {
            throw new \Exception('Invalid instruction');
        }
    }

    private function rectangle($a, $b)
    {
        for ($y = 0; $y < $b; $y++) {
            for ($x = 0; $x < $a; $x++) {
                $this->display[$y][$x] = true;
            }
        }
    }

    private function rotate($direction, $position, $value)
    {
        switch ($direction) {
            case 'column':
                $this->rotateColumn($position, $value);
                break;
            case 'row':
                $this->rotateRow($position, $value);
                break;
            default:
                throw new \Exception('Invalid direction');
        }

    }

    private function rotateRow($position, $value)
    {
        for ($i = 0; $i < $this->displayX - $value; $i++) {
            array_push($this->display[$position], array_shift($this->display[$position]));
        }
    }

    private function rotateColumn($position, $value)
    {
        /** Load column */
        $column = [];
        for ($i = 0; $i < $this->displayY; $i++) {
            $column[] = $this->display[$i][$position];
        }
        /** Rotate column */
        for ($i = 0; $i < $this->displayY - $value; $i++) {
            array_push($column, array_shift($column));
        }
        /** Overwrite display column with shifted values */
        for ($i = 0; $i < $this->displayY; $i++) {
            $this->display[$i][$position] = $column[$i];
        }
    }

    private function prepareDisplay()
    {
        $columns = [];
        for ($y = 0; $y < $this->displayY; $y++) {
            $row = [];
            for ($x = 0; $x < $this->displayX; $x++) {
                $row[] = false;
            }
            $columns[] = $row;
        }
        $this->display = $columns;
    }

    private function printDisplay()
    {
        for ($y = 0; $y < $this->displayY; $y++) {
            for ($x = 0; $x < $this->displayX; $x++) {
                if ($this->display[$y][$x]) {
                    echo '█';
                } else {
                    echo '░';
                }
            }
            echo "\n";
        }
    }

    private function countOnPixels()
    {
        $sum = 0;
        for ($y = 0; $y < $this->displayY; $y++) {
            for ($x = 0; $x < $this->displayX; $x++) {
                if ($this->display[$y][$x]) {
                    $sum++;
                }
            }
        }
        echo "\nSum: $sum\n";
    }
}

(new Day8())->run();