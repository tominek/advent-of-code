<?php

class Node
{
    private $x;
    private $y;
    private $distance;

    public function __construct($x, $y, $parent, $distance)
    {
        $this->x = $x;
        $this->y = $y;
        $this->distance = $distance;
    }

    /**
     * @return int
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * @return int
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * @return int
     */
    public function getDistance()
    {
        return $this->distance;
    }

    /**
     * @param int $favorite
     *
     * @return bool
     */
    public function isWall($favorite)
    {
        $x = $this->x;
        $y = $this->y;

        $sum = ($x * $x) + (3 * $x) + (2 * $x * $y) + $y + ($y * $y) + $favorite;
        $binary = decbin($sum);
        $count = substr_count($binary, '1');
        if ($count % 2 == 0) {
            return false;
        }
        return true;
    }

    /**
     * @param Node $node
     *
     * @return bool
     */
    public function equals($node)
    {
        if (
            $this->x == $node->getX()
            && $this->y == $node->getY()
        ) {
            return true;
        }
        return false;
    }
}