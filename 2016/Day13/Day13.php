<?php

require "Node.php";

class Day13
{
    private $favorite = 1352;
    /** @var Node[] */
    private $queue = [];

    public function run()
    {
        $root = new Node(1, 1, null, 0);
        $this->queue[] = $root;

        while (!empty($this->queue)) {
            $current = array_shift($this->queue);
            if ($current->getX() == 31 && $current->getY() == 39) {
                echo "## " . $current->getDistance() . " ##";
                return;
            }
            foreach ($this->getNeighbours($current) as $neighbour) {
                if (
                    !$neighbour->isWall($this->favorite)
                    && !$neighbour->equals($current)
                ) {
                    $this->queue[] = $neighbour;
                }
            }
        }
    }

    /**
     * @param Node $current
     *
     * @return Node[]
     */
    private function getNeighbours($current)
    {
        $neighbours = [];
        $curX = $current->getX();
        $curY = $current->getY();

        $neighbours[] = new Node($curX + 1, $curY, $current, $current->getDistance() + 1);
        $neighbours[] = new Node($curX - 1, $curY, $current, $current->getDistance() + 1);
        $neighbours[] = new Node($curX, $curY + 1, $current, $current->getDistance() + 1);
        $neighbours[] = new Node($curX, $curY - 1, $current, $current->getDistance() + 1);

        return $neighbours;
    }
}

(new Day13())->run();