<?php

class Day2
{
    private $lastNumber = 5;
    private $X = 1;
    private $Y = 3;

    public function run()
    {
        $lines = $this->getInput();
        foreach ($lines as $line) {
            foreach (str_split($line) as $char) {
                $this->moveFinger($char);
            }
            echo $this->getKeypad()[$this->X][$this->Y];
        }
    }

    private function moveFinger($char)
    {
        $moveX = 0;
        $moveY = 0;
        switch ($char) {
            case 'U':
                $moveY = -1;
                break;
            case 'D':
                $moveY = 1;
                break;
            case 'R':
                $moveX = 1;
                break;
            case 'L':
                $moveX = -1;
                break;
            default:
                return;
        }
        if (isset($this->getKeypad()[$this->X + $moveX][$this->Y + $moveY])) {
            $this->X += $moveX;
            $this->Y += $moveY;
        }
    }

    private function getInput()
    {
        $instructions = [];
        $test = fopen("test", "r");
        while (!feof($test)) {
            $instructions[] = fgets($test);
        }
        return $instructions;
    }

    private function getKeypad()
    {
//        $keypad[1][1] = 1;
//        $keypad[2][1] = 2;
//        $keypad[3][1] = 3;
//        $keypad[1][2] = 4;
//        $keypad[2][2] = 5;
//        $keypad[3][2] = 6;
//        $keypad[1][3] = 7;
//        $keypad[2][3] = 8;
//        $keypad[3][3] = 9;

        $keypad[3][1] = 1;
        $keypad[2][2] = 2;
        $keypad[3][2] = 3;
        $keypad[4][2] = 4;
        $keypad[1][3] = 5;
        $keypad[2][3] = 6;
        $keypad[3][3] = 7;
        $keypad[4][3] = 8;
        $keypad[5][3] = 9;
        $keypad[2][4] = 'A';
        $keypad[3][4] = 'B';
        $keypad[4][4] = 'C';
        $keypad[3][5] = 'D';

        return $keypad;
    }
}

(new Day2())->run();