<?php

class Day4
{
    private function getInput()
    {
        $rooms = [];
        $test = fopen("test", "r");
        while (!feof($test)) {
            preg_match('/([a-z-]+)(\d+)\[([a-z]+)\]/', fgets($test), $matches);
            $rooms[] = $matches;
        }
        return $rooms;
    }

    public function run()
    {
        $sum = 0;

        $rooms = $this->getInput();
        foreach ($rooms as $room) {
            $name = $room[1];
            $number = $room[2];
            $checksum = $room[3];

            if (
            $this->roomIsValid($name, $checksum)
//                && $this->checksumIsValid($checksum)
            ) {
                $sum += $number;
            }
        }
        echo $sum;
    }

    private function checksumIsValid($checksum)
    {
        $previous = null;

        foreach (str_split($checksum) as $char) {
            if (strcmp($previous, $char) > 0) {
                return false;
            }
        }

        return true;
    }

    private function roomIsValid($name, $checksum)
    {
//        $prevCount = 200;
//        $prevChar = null;
        $name = str_replace('-', '', $name);
        $charCount = count_chars($name, 3);
        if (substr($charCount, 0, 5) == $checksum) return true;


//        foreach (str_split($checksum) as $char) {
//            $count = substr_count($name, $char);
//
//            if ($count < 1) {
//                $prevChar = $char;
//                continue;
//            }
//            if ($count > $prevCount) {
//                return false;
//            }
//            if ($count == $prevCount && strcmp($prevChar, $char) > 0) {
//                return false;
//            }
//            $prevCount = $count;
//            $prevChar = $char;
//        }
        return false;
    }
}

(new Day4())->run();
//echo substr("aaaaabbbzyx", 0, 5);
//var_dump(count_chars("bqxnfdmhbbzmcxbnzshmfcdozqsldms", 3));

//$name = str_replace('-', '', "not-a-real-room-");
//$charCount = count_chars($name, 3);
//
//echo substr($charCount, 0, 5);